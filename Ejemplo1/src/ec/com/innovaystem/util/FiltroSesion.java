package ec.com.innovaystem.util;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

public class FiltroSesion implements Filter {
	@Resource
	UserTransaction tx;

	String userTransactionJndiName;
	public static final String USER_TRANSACTION_JNDI_NAME_ATTR = "my.commons.web.USER_TRANSACTION_JNDI_NAME";

	public FiltroSesion() {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (isObjetoEstatico(request)) {
			chain.doFilter(request, response);
		} else {
			HttpSession session;
			// HttpServletRequest httprequest = ((HttpServletRequest)request);
			session = ((HttpServletRequest) request).getSession();
			try {
				if (session.getAttribute("usuario") == null) {
					((HttpServletResponse) response).sendRedirect("/Ejemplo1/login.xhtml");
				} else {
					if (session.getAttribute("usuario") != null) {
						tx.begin();
						chain.doFilter(request, response);
						if (!FiltroEJB.isMarcaRollback())
							tx.commit();
						else
							tx.rollback();
					} 
				}
			} catch (Throwable e) {
				if (tx != null) {
					try {
						tx.rollback();
					} catch (Exception e1) {
						e1.printStackTrace();
						RequestDispatcher dispatcherror = request.getRequestDispatcher("/access-denied.xhtml");
						dispatcherror.forward(request, response);

					}
				}

				throw new ServletException(e);
			} finally {
				FiltroEJB.limpiarMarcaRollback();
			}
		}
	}

	private boolean isObjetoEstatico(ServletRequest request) {
		String lRequestURI = getRequestURI(request);
		boolean tru = lRequestURI.endsWith(".gif") || lRequestURI.endsWith(".jpg") || lRequestURI.endsWith(".html")
				|| lRequestURI.endsWith(".js") || lRequestURI.endsWith(".css") || lRequestURI.endsWith(".png") ||

				lRequestURI.trim().endsWith("css.xhtml") ||
				lRequestURI.trim().endsWith("js.xhtml") ||
				lRequestURI.trim().endsWith("ttf.xhtml") ||
				lRequestURI.trim().endsWith("woff2.xhtml") ||
				lRequestURI.trim().endsWith("woff.xhtml") ||
				lRequestURI.trim().endsWith("svg.xhtml") ||
				lRequestURI.trim().endsWith("png.xhtml") ||
				lRequestURI.trim().endsWith("jpg.xhtml") ||
				lRequestURI.trim().endsWith("eot.xhtml") ||
				lRequestURI.trim().endsWith("gif.xhtml") ||
				
				lRequestURI.endsWith(".ttf") || lRequestURI.endsWith(".woff2") || lRequestURI.endsWith(".woff")
				|| lRequestURI.endsWith(".svg") || lRequestURI.endsWith(".eot") ||

				lRequestURI.endsWith("login.xhtml");
		return tru;
	}

	private String getRequestURI(ServletRequest request) {
		String ls_url = "";
		if (request instanceof HttpServletRequest)
			ls_url = ((HttpServletRequest) request).getRequestURI();
		return ls_url;
	}

	public void init(FilterConfig pConfig) throws ServletException {
		// userTransactionJndiName=pConfig.getInitParameter(USER_TRANSACTION_JNDI_NAME_ATTR);
	}
}