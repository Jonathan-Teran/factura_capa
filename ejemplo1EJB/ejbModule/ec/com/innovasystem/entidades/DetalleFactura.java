package ec.com.innovasystem.entidades;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the detalle_factura database table.
 * 
 */
@Entity
@Table(name="detalle_factura")
@NamedQuery(name="DetalleFactura.findAll", query="SELECT d FROM DetalleFactura d")
public class DetalleFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="detalle_factura_id")
	private int detalleFacturaId;

	private int cantidad;

	@Column(name="valor_descuento")
	private Float valorDescuento;

	@Column(name="valor_neto")
	private Float valorNeto;

	//bi-directional many-to-one association to Factura
	@ManyToOne
	@JoinColumn(name="factura_id_det") 
	private Factura factura;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="producto_id")
	private Producto producto;

	//bi-directional many-to-one association to TipoImpuesto
	@ManyToOne
	@JoinColumn(name="tipo_impuesto")
	private TipoImpuesto tipoImpuestoBean;

	public DetalleFactura() {
	}

	public int getDetalleFacturaId() {
		return this.detalleFacturaId;
	}

	public void setDetalleFacturaId(int detalleFacturaId) {
		this.detalleFacturaId = detalleFacturaId;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Float getValorDescuento() {
		return this.valorDescuento;
	}

	public void setValorDescuento(Float valorDescuento) {
		this.valorDescuento = valorDescuento;
	}

	public Float getValorNeto() {
		return this.valorNeto;
	}

	public void setValorNeto(Float valorNeto) {
		this.valorNeto = valorNeto;
	}

	public Factura getFactura() {
		return this.factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public TipoImpuesto getTipoImpuestoBean() {
		return this.tipoImpuestoBean;
	}

	public void setTipoImpuestoBean(TipoImpuesto tipoImpuestoBean) {
		this.tipoImpuestoBean = tipoImpuestoBean;
	}

}