package ec.com.innovaystem.util;

import java.util.Date;

public class Usuario {

	private String usuario;
	private String clave;
	private Date fechaIngreso;
	
	public Usuario(){
	}
	
	public Usuario(String usuario, String clave){
		this.usuario = usuario;
		this.clave = clave;
		this.fechaIngreso = new Date();
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	@Override
	public String toString() {
		return "Usuario [usuario=" + usuario + ", clave=" + clave + ", fechaIngreso=" + fechaIngreso + "]";
	}
	
	
}
