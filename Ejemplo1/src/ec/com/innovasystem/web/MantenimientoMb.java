package ec.com.innovasystem.web;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ec.com.innovasystem.entidades.Cliente;
import ec.com.innovasystem.entidades.DetalleFactura;
import ec.com.innovasystem.entidades.Factura;
import ec.com.innovasystem.entidades.Marca;
import ec.com.innovasystem.entidades.Producto;
import ec.com.innovasystem.entidades.TipoImpuesto;
import ec.com.innovasystem.entidades.TipoProducto;
import ec.innovasystem.dao.ClienteDao;
import ec.innovasystem.dao.DetalleFacturaDao;
import ec.innovasystem.dao.FacturaDao;
import ec.innovasystem.dao.MarcaDao;
import ec.innovasystem.dao.ProductoDao;
import ec.innovasystem.dao.TipoImpuestoDao;
import ec.innovasystem.dao.TipoProductoDao;

@ManagedBean
@SessionScoped
public class MantenimientoMb {
	
	private TipoProducto tipoProducto;
	private TipoProducto tipoProductoConsulta;
	private Integer idBusqueda;
	public List<TipoProducto> lstTipoProducto;

	private Producto productoNuevo;
	private Producto productoConsulta;
	private Integer idProductoConsulta;
	public List<Producto> lstProducto;
	
	private TipoImpuesto tipoImpuestoNuevo;
	private TipoImpuesto tipoImpuestoConsulta;
	private Integer idTipoImpuesto;
	public List<TipoImpuesto> lstTipoImpuesto;
	
	private Cliente clienteNuevo;
	private Cliente clienteConsulta;
	private Integer idCliente;
	public List<Cliente> lstCliente;
	private String mensajeValida;
	
	
	private Factura facturaNueva;
	private Factura facturaConsulta;
	private Integer idFactura;
	public List<Factura> lstFactura;
	
	private DetalleFactura detalleFacturaNueva;
	private DetalleFactura detalleFacturaConsulta;
	private Integer idDetalleFactura;
	public List<DetalleFactura> lstDetalleFactura;
	
	private Marca marcaNueva;
	private Marca marcaConsulta;
	private Integer idMarca;
	public List<Marca> lstMarca;
	
	
	public MantenimientoMb() {
		
	}
	@ManagedProperty(value = "#{loginMb}")
	private LoginMb loginMb;
	
	public LoginMb getLoginMb() {
		return loginMb;
	}

	public void setLoginMb(LoginMb loginMb) {
		this.loginMb = loginMb;
	}

	@PostConstruct
	
	public void inicializar(){
	
		if(loginMb!=null)
			System.out.println(loginMb.getUsuario());
		else
			System.out.println("Sin sesion");
		
	tipoProducto = new TipoProducto();
	productoNuevo =new Producto();
	tipoImpuestoNuevo =new TipoImpuesto();
	clienteNuevo =new Cliente();
	facturaNueva =new Factura();
	detalleFacturaNueva =new DetalleFactura();
	marcaNueva =new Marca();
	
	this.consultarTodos();
	this.consultarTodosCliente();
	this.consultarTodosDetalleFactura();
	this.consultarTodosProducto();
	this.consultarTodosTipoImpuesto();
	this.consultarTodosFactura();
	this.consultarTodosMarca();
	}
	@EJB
	TipoProductoDao tipoProductoDao;
	@EJB
	ProductoDao productoDao;
	@EJB
	TipoImpuestoDao tipoImpuestoDao;
	@EJB
	ClienteDao clienteDao;
	@EJB
	FacturaDao facturaDao;
	@EJB
	DetalleFacturaDao detalleFacturaDao;
	@EJB
	MarcaDao marcaDao;


	public void validarAjax() {
		mensajeValida=ValidarIdentificacion.validarIdentificacion(this.clienteNuevo.getIdentificacion());
		
		if (mensajeValida != null) {
			mostrarMensaje("Mensaje", "Cedula Incorrecta", FacesMessage.SEVERITY_FATAL);
		}else{
			
			
			mostrarMensaje("Mensaje", "Cedula Correcta", FacesMessage.SEVERITY_INFO);
			
		}
	}
	public void crear() {
		if(this.tipoProducto.getNombre()==null||this.tipoProducto.getNombre().isEmpty()){
			mostrarMensaje("ERROR", 
					"Datos vacios", FacesMessage.SEVERITY_ERROR);
			return;
		}
		
		try {
			tipoProductoDao.crear(tipoProducto);
			mostrarMensaje("Exito", "Se ha guardado exitosamente el Tipo de Producto" + tipoProducto.getNombre(),
					FacesMessage.SEVERITY_INFO);
			tipoProducto = new TipoProducto();
			consultarTodos();
		} catch (Exception e) {
			mostrarMensaje("ERROR", "Error al guardar el Tipo de Producto", FacesMessage.SEVERITY_ERROR);
			e.printStackTrace();
		}
	}

	public void consultarTodos(){
		try {
			lstTipoProducto = tipoProductoDao.consultarTodosTipoProducto();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/*
	 * public void consultarTipoProductoXPk(){
	 * if(tipoProducto!=null&&tipoProducto.getNombre()!=null){ try { Integer pk
	 * = Integer.getInteger(tipoProducto.getNombre()); this.tipoProducto =
	 * tipoProductoDao.buscarPorPK(pk); } catch (Exception e) {
	 * e.printStackTrace(); } } }
	 */

	public void consultarTipoProductoXPk() {
		if (idBusqueda != null) {
			try {
				this.tipoProductoConsulta = tipoProductoDao.buscarPorPK(idBusqueda);

			} catch (Exception e) {

			}
		}
	}

	public void actualizarTipoProducto(){
		if(this.tipoProductoConsulta!=null){
			try {
				tipoProductoDao.actualizar(tipoProductoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha actualizado Tipo de producto "+tipoProductoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				tipoProductoConsulta = new TipoProducto();
				consultarTodos();
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al actualizar Tipo de producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}

	public void eliminarTipoProducto() {
		seleccionarElemento();
		if (tipoProductoConsulta != null) {

			try {
				tipoProductoDao.eliminar(tipoProductoConsulta);
				mostrarMensaje("Informacion",
						"Se ha eliminado exitosamente el Tipo de Producto" + tipoProducto.getNombre(),
						FacesMessage.SEVERITY_INFO);
				tipoProductoConsulta = new TipoProducto();

				consultarTodos();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	
	public void seleccionarElemento(){
		this.tipoProductoConsulta = (TipoProducto) valorContextoJSF("#{tipoProducto}", TipoProducto.class); 
	}

	
	
	
	
	
	//inicia producto********************------------*******************--------------******************------------*******////
	public void crearProducto() {
		try {
			productoDao.crear(productoNuevo);
			mostrarMensaje("Exito", "Se ha guardado exitosamente el Producto" + productoNuevo.getNombre(),
					FacesMessage.SEVERITY_INFO);
			productoNuevo = new Producto();
			consultarTodosProducto();
		} catch (Exception e) {
			mostrarMensaje("ERROR", "Error al guardar el Tipo de Producto", FacesMessage.SEVERITY_ERROR);
			e.printStackTrace();
		}
	}

	public void consultarTodosProducto(){
		try {
			lstProducto = productoDao.consultarTodosProducto();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void consultarProductoXPk(){
		if(idProductoConsulta!=null){
			try {
				this.productoConsulta = productoDao.buscarPorPK(idProductoConsulta);
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al consultar producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	
	public void actualizarProducto(){
		if(this.productoConsulta!=null){
			try {
				productoDao.actualizar(productoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha actualizado producto "+productoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				productoConsulta = new Producto();
				consultarTodos();
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al actualizar producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	public void eliminarProducto(){
		seleccionarProducto();
		if(productoConsulta!=null){
			try {
				productoDao.eliminar(productoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha eliminado producto "+productoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				productoConsulta = new Producto();
				consultarTodosProducto();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void seleccionarProducto(){
		this.productoConsulta = (Producto) valorContextoJSF("#{producto}", Producto.class); 
	}
	
//fin de producto
	
	
	
	//inicia tipo impuesto********************------------*******************--------------******************------------*******////
	public void crearTipoImpuesto() {
		try {
			tipoImpuestoDao.crear(tipoImpuestoNuevo);
			mostrarMensaje("Exito", "Se ha guardado exitosamente el Tipo impuesto" + tipoImpuestoNuevo.getNombre(),
					FacesMessage.SEVERITY_INFO);
			tipoImpuestoNuevo = new TipoImpuesto();
			consultarTodosTipoImpuesto();
		} catch (Exception e) {
			mostrarMensaje("ERROR", "Error al guardar el Tipo de Producto", FacesMessage.SEVERITY_ERROR);
			e.printStackTrace();
		}
	}

	public void consultarTodosTipoImpuesto(){
		try {
			lstTipoImpuesto = tipoImpuestoDao.consultarTodosTipoImpuesto();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void consultarTipoImpuestoXPk(){
		if(idTipoImpuesto!=null){
			try {
				this.tipoImpuestoConsulta = tipoImpuestoDao.buscarPorPK(idTipoImpuesto);
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al consultar producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	
	
	public void actualizarTipoImpuesto(){
		if(this.tipoImpuestoConsulta!=null){
			try {
				tipoImpuestoDao.actualizar(tipoImpuestoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha actualizado producto "+tipoImpuestoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				tipoImpuestoConsulta = new TipoImpuesto();
				consultarTodosTipoImpuesto();
			} catch (Exception e) {
				mostrarMensaje("ERROR", 
						"Error al actualizar producto", FacesMessage.SEVERITY_ERROR);
			}
		}
	}
	 
	public void eliminarTipoImpuesto(){
		seleccionarTipoImpuesto();
		if(tipoImpuestoConsulta!=null){
			try {
				tipoImpuestoDao.eliminar(tipoImpuestoConsulta);
				mostrarMensaje("Informacion", 
						"Se ha eliminado producto "+tipoImpuestoConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
				tipoImpuestoConsulta = new TipoImpuesto();
				 consultarTodosTipoImpuesto();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void seleccionarTipoImpuesto(){
		this.tipoImpuestoConsulta = (TipoImpuesto) valorContextoJSF("#{tipoImpuesto}", TipoImpuesto.class); 
	}
	
//fin de tipo impuesto --------------------*******************--------------*******************--------
	
	
	//inicia CLIENTE********************------------*******************--------------******************------------*******////
		public void crearCliente() {
			try {
				
				clienteDao.crear(clienteNuevo);
				mostrarMensaje("Exito", "Se ha guardado exitosamente el Cliente" + clienteNuevo.getNombre(),
						FacesMessage.SEVERITY_INFO);
				clienteNuevo = new Cliente();
				consultarTodosCliente();
			} catch (Exception e) {
				mostrarMensaje("ERROR", "Error al guardar el Cliente", FacesMessage.SEVERITY_ERROR);
				e.printStackTrace();
			}
		}
	
		
		
		
		
		

		public void consultarTodosCliente(){
			try {
				lstCliente = clienteDao.consultarTodosCliente();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		public void consultarClienteXPk(){
			if(idCliente!=null){
				try {
					this.clienteConsulta = clienteDao.buscarPorPK(idCliente);
				} catch (Exception e) {
					mostrarMensaje("ERROR", 
							"Error al consultar Cliente", FacesMessage.SEVERITY_ERROR);
				}
			}
		}
		
		
		public void actualizarCliente(){
			if(this.clienteConsulta!=null){
				try {
					clienteDao.actualizar(clienteConsulta);
					mostrarMensaje("Informacion", 
							"Se ha actualizado Cliente "+clienteConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
					clienteConsulta = new Cliente();
					consultarTodosCliente();
				} catch (Exception e) {
					mostrarMensaje("ERROR", 
							"Error al actualizar Cliente", FacesMessage.SEVERITY_ERROR);
				}
			}
		}
		 
		public void eliminarCliente(){
			seleccionarCliente();
			if(clienteConsulta!=null){
				try {
					clienteDao.eliminar(clienteConsulta);
					
					mostrarMensaje("Informacion", 
							"Se ha eliminado cliente "+clienteConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
					clienteConsulta = new Cliente();
					
					 consultarTodosCliente();
					 
				} catch (Exception e) {
					mostrarMensaje("ERROR", 
							"Error al ELIMINAR Cliente", FacesMessage.SEVERITY_ERROR);
				}
				
			}
		}
		
		public void seleccionarCliente(){
			this.clienteConsulta = (Cliente) valorContextoJSF("#{cliente}", Cliente.class); 
		}
		
	//fin de CLIENTE --------------------*******************--------------*******************--------
	
		
		//inicia FACTURA********************------------*******************--------------******************------------*******////
				public void crearFactura() {
					try {
						facturaDao.crear(facturaNueva);
						mostrarMensaje("Exito", "Se ha guardado exitosamente la factura" + facturaNueva.getFacturaId(),
								FacesMessage.SEVERITY_INFO);
						facturaNueva = new Factura();
						consultarTodosFactura();
					} catch (Exception e) {
						mostrarMensaje("ERROR", "Error al guardar la factura", FacesMessage.SEVERITY_ERROR);
						e.printStackTrace();
					}
				}

				public void consultarTodosFactura(){
					try {
						lstFactura = facturaDao.consultarTodosFactura();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				public void consultarFacturaXPk(){
					if(idFactura!=null){
						try {
							this.facturaConsulta = facturaDao.buscarPorPK(idFactura);
						} catch (Exception e) {
							mostrarMensaje("ERROR", 
									"Error al consultar factura", FacesMessage.SEVERITY_ERROR);
						}
					}
				}
				
				
				public void actualizarFactura(){
					if(this.facturaConsulta!=null){
						try {
							facturaDao.actualizar(facturaConsulta);
							mostrarMensaje("Informacion", 
									"Se ha actualizado Factura "+facturaConsulta.getFacturaId(), FacesMessage.SEVERITY_INFO);
							facturaConsulta = new Factura();
							consultarTodosFactura();
						} catch (Exception e) {
							mostrarMensaje("ERROR", 
									"Error al actualizar factura", FacesMessage.SEVERITY_ERROR);
						}
					}
				}
				 
				public void eliminarFactura(){
					seleccionarFactura();
					if(facturaConsulta!=null){
						try {
							facturaDao.eliminar(facturaConsulta);
							mostrarMensaje("Informacion", 
									"Se ha eliminado factura "+facturaConsulta.getFacturaId(), FacesMessage.SEVERITY_INFO);
							facturaConsulta = new Factura();
							 consultarTodosFactura();
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
				}
				
				public void seleccionarFactura(){
					this.facturaConsulta = (Factura) valorContextoJSF("#{factura}", Factura.class); 
				}
				
			//fin de CLIENTE --------------------*******************--------------*******************--------
			
				
				//inicia Detalle FACTURA********************------------*******************--------------******************------------*******////
				public void crearDetalleFactura() {
					try {
						detalleFacturaDao.crear(detalleFacturaNueva);
						mostrarMensaje("Exito", "Se ha guardado exitosamente la Detalle factura" + detalleFacturaNueva.getDetalleFacturaId(),
								FacesMessage.SEVERITY_INFO);
						detalleFacturaNueva = new DetalleFactura();
						consultarTodosDetalleFactura();
					} catch (Exception e) {
						mostrarMensaje("ERROR", "Error al guardar el detalle factura", FacesMessage.SEVERITY_ERROR);
						e.printStackTrace();
					}
				}

				public void consultarTodosDetalleFactura(){
					try {
						lstDetalleFactura = detalleFacturaDao.consultarTodosDetalleFactura();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				public void consultarDetalleFacturaXPk(){
					if(idDetalleFactura!=null){
						try {
							this.detalleFacturaConsulta = detalleFacturaDao.buscarPorPK(idDetalleFactura);
						} catch (Exception e) {
							mostrarMensaje("ERROR", 
									"Error al consultar detalle factura", FacesMessage.SEVERITY_ERROR);
						}
					}
				}
				
				
				public void actualizarDetalleFactura(){
					if(this.detalleFacturaConsulta!=null){
						try {
							detalleFacturaDao.actualizar(detalleFacturaConsulta);
							mostrarMensaje("Informacion", 
									"Se ha actualizado detalle Factura "+detalleFacturaConsulta.getDetalleFacturaId(), FacesMessage.SEVERITY_INFO);
							detalleFacturaConsulta = new DetalleFactura();
							consultarTodosDetalleFactura();
						} catch (Exception e) {
							mostrarMensaje("ERROR", 
									"Error al actualizar detalle factura", FacesMessage.SEVERITY_ERROR);
						}
					}
				}
				 
				public void eliminarDetalleFactura(){
					seleccionarDetalleFactura();
					if(detalleFacturaConsulta!=null){
						try {
							detalleFacturaDao.eliminar(detalleFacturaConsulta);
							mostrarMensaje("Informacion", 
									"Se ha eliminado detalle factura "+detalleFacturaConsulta.getDetalleFacturaId(), FacesMessage.SEVERITY_INFO);
							detalleFacturaConsulta = new DetalleFactura();
							 consultarTodosDetalleFactura();
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
				}
				
				public void seleccionarDetalleFactura(){
					this.detalleFacturaConsulta = (DetalleFactura) valorContextoJSF("#{detalleFactura}", DetalleFactura.class); 
				}
				
			//fin de detalle CLIENTE --------------------*******************--------------*******************--------
			
				//inicia Marca********************------------*******************--------------******************------------*******////
				public void crearMarca() {
					try {
						marcaDao.crear(marcaNueva);
						mostrarMensaje("Exito", "Se ha guardado exitosamente la marca" + marcaNueva.getNombre(),
								FacesMessage.SEVERITY_INFO);
						marcaNueva = new Marca();
						consultarTodosMarca();
					} catch (Exception e) {
						mostrarMensaje("ERROR", "Error al guardar la marca", FacesMessage.SEVERITY_ERROR);
						e.printStackTrace();
					}
				}

				public void consultarTodosMarca(){
					try {
						lstMarca = marcaDao.consultarTodosMarca();
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
				
				public void consultarMarcaXPk(){
					if(idMarca!=null){
						try {
							this.marcaConsulta = marcaDao.buscarPorPK(idMarca);
						} catch (Exception e) {
							mostrarMensaje("ERROR", 
									"Error al consultar la marca", FacesMessage.SEVERITY_ERROR);
						}
					}
				}
				
				
				public void actualizarMarca(){
					if(this.marcaConsulta!=null){
						try {
							marcaDao.actualizar(marcaConsulta);
							mostrarMensaje("Informacion", 
									"Se ha actualizado la marca"+marcaConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
							marcaConsulta = new Marca();
							consultarTodosMarca();
						} catch (Exception e) {
							mostrarMensaje("ERROR", 
									"Error al actualizar marca", FacesMessage.SEVERITY_ERROR);
						}
					}
				}
				 
				public void eliminarMarca(){
					seleccionarMarca();
					if(marcaConsulta!=null){
						try {
							marcaDao.eliminar(marcaConsulta);
							mostrarMensaje("Informacion", 
									"Se ha eliminado la marca "+marcaConsulta.getNombre(), FacesMessage.SEVERITY_INFO);
							marcaConsulta = new Marca();
							 consultarTodosMarca();
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
				}
				
				public void seleccionarMarca(){
					this.marcaConsulta = (Marca) valorContextoJSF("#{marca}", Marca.class); 
				}
				
			//fin de detalle CLIENTE --------------------*******************--------------*******************--------
				
				
//	/**
//	 * M�todo exclusivo de tablas, devuelve el valor de una tabla en la cual el
//	 * est�ndar es poner el iterador con nombre obj #{obj}, solo se obtiene de
//	 * tablas de JSF.
//	 * 
//	 * @param claseEsperada
//	 *            Tipo de objeto en la cual se espera que retorne el objeto
//	 * @return Objeto encontrado o nulo si no existe en el contexto o se env�an
//	 *         mal los par�metros
//	 */
//	public static <T> Object valorContextoJSF(Class<T> claseEsperada) {
//		Object obj = null;
//		if (claseEsperada != null)
//			obj = FacesContext.getCurrentInstance().getApplication()
//					.evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{tipoProducto}", claseEsperada);
//		return obj;
//	}

	/**
	 * M�todo exclusivo de tablas, devuelve el valor del contexto de la
	 * aplicaci�n, debe enviarse el valor con la respectiva sintaxis jsf
	 * #{nombreVaribableJSF}
	 * 
	 * @param claseEsperada
	 *            Tipo de objeto en la cual se espera que retorne el objeto
	 * @param expresion
	 *            Expresion para realizar la b�squeda en el contexto, debe tener
	 *            la sintaxis #{nombreVaribableJSF}
	 * @return Objeto encontrado o nulo si no existe en el contexto o se env�an
	 *         mal los par�metros
	 */
	public static <T> Object valorContextoJSF(String expresion, Class<T> claseEsperada) {
		Object obj = null;
		if (claseEsperada != null)
			obj = FacesContext.getCurrentInstance().getApplication()
					.evaluateExpressionGet(FacesContext.getCurrentInstance(), expresion, claseEsperada);
		return obj;
	}

	private void mostrarMensaje(String cabecera, String mensaje, Severity tipo) {
		FacesMessage message = new FacesMessage(tipo, cabecera, mensaje);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public TipoProducto getTipoProductoConsulta() {
		return tipoProductoConsulta;
	}

	public void setTipoProductoConsulta(TipoProducto tipoProductoConsulta) {
		this.tipoProductoConsulta = tipoProductoConsulta;

	}

	public List<TipoProducto> getLstTipoProducto() {
		return lstTipoProducto;
	}

	public void setLstTipoProducto(List<TipoProducto> lstTipoProducto) {
		this.lstTipoProducto = lstTipoProducto;
	}

	public Integer getIdBusqueda() {
		return idBusqueda;
	}

	public void setIdBusqueda(Integer idBusqueda) {
		this.idBusqueda = idBusqueda;
	}

	public TipoProducto getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(TipoProducto tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	public Producto getProductoNuevo() {
		return productoNuevo;
	}
	public void setProductoNuevo(Producto productoNuevo) {
		this.productoNuevo = productoNuevo;
	}
	public Producto getProductoConsulta() {
		return productoConsulta;
	}
	public void setProductoConsulta(Producto productoConsulta) {
		this.productoConsulta = productoConsulta;
	}
	public Integer getIdProductoConsulta() {
		return idProductoConsulta;
	}
	public void setIdProductoConsulta(Integer idProductoConsulta) {
		this.idProductoConsulta = idProductoConsulta;
	}
	public List<Producto> getLstProducto() {
		return lstProducto;
	}
	public void setLstProducto(List<Producto> lstProducto) {
		this.lstProducto = lstProducto;
	}
	public TipoImpuesto getTipoImpuestoNuevo() {
		return tipoImpuestoNuevo;
	}
	public void setTipoImpuestoNuevo(TipoImpuesto tipoImpuestoNuevo) {
		this.tipoImpuestoNuevo = tipoImpuestoNuevo;
	}
	public TipoImpuesto getTipoImpuestoConsulta() {
		return tipoImpuestoConsulta;
	}
	public void setTipoImpuestoConsulta(TipoImpuesto tipoImpuestoConsulta) {
		this.tipoImpuestoConsulta = tipoImpuestoConsulta;
	}
	public Integer getIdTipoImpuesto() {
		return idTipoImpuesto;
	}
	public void setIdTipoImpuesto(Integer idTipoImpuesto) {
		this.idTipoImpuesto = idTipoImpuesto;
	}
	public List<TipoImpuesto> getLstTipoImpuesto() {
		return lstTipoImpuesto;
	}
	public void setLstTipoImpuesto(List<TipoImpuesto> lstTipoImpuesto) {
		this.lstTipoImpuesto = lstTipoImpuesto;
	}
	public Cliente getClienteNuevo() {
		return clienteNuevo;
	}
	public void setClienteNuevo(Cliente clienteNuevo) {
		this.clienteNuevo = clienteNuevo;
	}
	public Cliente getClienteConsulta() {
		return clienteConsulta;
	}
	public void setClienteConsulta(Cliente clienteConsulta) {
		this.clienteConsulta = clienteConsulta;
	}
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public List<Cliente> getLstCliente() {
		return lstCliente;
	}
	public void setLstCliente(List<Cliente> lstCliente) {
		this.lstCliente = lstCliente;
	}
	public Factura getFacturaNueva() {
		return facturaNueva;
	}
	public void setFacturaNueva(Factura facturaNueva) {
		this.facturaNueva = facturaNueva;
	}
	public Factura getFacturaConsulta() {
		return facturaConsulta;
	}
	public void setFacturaConsulta(Factura facturaConsulta) {
		this.facturaConsulta = facturaConsulta;
	}
	public Integer getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(Integer idFactura) {
		this.idFactura = idFactura;
	}
	public List<Factura> getLstFactura() {
		return lstFactura;
	}
	public void setLstFactura(List<Factura> lstFactura) {
		this.lstFactura = lstFactura;
	}
	public DetalleFactura getDetalleFacturaNueva() {
		return detalleFacturaNueva;
	}
	public void setDetalleFacturaNueva(DetalleFactura detalleFacturaNueva) {
		this.detalleFacturaNueva = detalleFacturaNueva;
	}
	public DetalleFactura getDetalleFacturaConsulta() {
		return detalleFacturaConsulta;
	}
	public void setDetalleFacturaConsulta(DetalleFactura detalleFacturaConsulta) {
		this.detalleFacturaConsulta = detalleFacturaConsulta;
	}
	public Integer getIdDetalleFactura() {
		return idDetalleFactura;
	}
	public void setIdDetalleFactura(Integer idDetalleFactura) {
		this.idDetalleFactura = idDetalleFactura;
	}
	public List<DetalleFactura> getLstDetalleFactura() {
		return lstDetalleFactura;
	}
	public void setLstDetalleFactura(List<DetalleFactura> lstDetalleFactura) {
		this.lstDetalleFactura = lstDetalleFactura;
	}
	public Marca getMarcaNueva() {
		return marcaNueva;
	}
	public void setMarcaNueva(Marca marcaNueva) {
		this.marcaNueva = marcaNueva;
	}
	public Marca getMarcaConsulta() {
		return marcaConsulta;
	}
	public void setMarcaConsulta(Marca marcaConsulta) {
		this.marcaConsulta = marcaConsulta;
	}
	public Integer getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}
	public List<Marca> getLstMarca() {
		return lstMarca;
	}
	public void setLstMarca(List<Marca> lstMarca) {
		this.lstMarca = lstMarca;
	}

	public String getMensajeValida() {
		return mensajeValida;
	}

	public void setMensajeValida(String mensajeValida) {
		this.mensajeValida = mensajeValida;
	}
	
}