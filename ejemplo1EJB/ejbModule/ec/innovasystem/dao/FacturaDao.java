package ec.innovasystem.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.com.innovasystem.entidades.Factura;



/**
 * Session Bean implementation class TipoProductoDao
 */
@Stateless
@LocalBean
public class FacturaDao extends DAO<Factura, Integer> {

    /**
     * Default constructor. 
     */
    public FacturaDao() {
        // TODO Auto-generated constructor stub
    }
    
    public void crearFactura (Factura factura){
    	try {
			crear(factura);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    public Factura consultarFactura(Integer id){
		try{
		return buscarPorPK(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Factura> consultarTodosFactura(){
		List<Factura> lst=null;
		try{
			Query q=getNamedQuery("Factura.findAll"); 
			lst=(List<Factura>) q.getResultList();
			}catch(Exception e){
				
			}
		return lst;
	}
    

}
