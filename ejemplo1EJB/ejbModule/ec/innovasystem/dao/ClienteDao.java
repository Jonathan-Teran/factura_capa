package ec.innovasystem.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.com.innovasystem.entidades.Cliente;




/**
 * Session Bean implementation class TipoProductoDao
 */
@Stateless
@LocalBean
public class ClienteDao extends DAO<Cliente, Integer> {

    /**
     * Default constructor. 
     */
    public ClienteDao() {
        // TODO Auto-generated constructor stub
    }
    
    public void crearCliente (Cliente cliente){
    	try {
			crear(cliente);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    public Cliente consultarCliente(Integer id){
		try{
		return buscarPorPK(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
    @SuppressWarnings("unchecked")
	public List<Cliente> consultarTodosCliente(){
		List<Cliente> lst=null;
		try{
			Query q=getNamedQuery("Cliente.findAll"); 
			lst=(List<Cliente>) q.getResultList();
			}catch(Exception e){
				
			}
		return lst;
	}
    
	 @SuppressWarnings("unchecked")
		public List<Cliente> buscarXNombre(String nombre){
	    	List<Cliente> lst = null;
	    	try {
	    		Query q = getNamedQuery("Cliente.buscarXNombre");
	    		q.setParameter("nombre", nombre);
				lst = (List<Cliente>) q.getResultList();
			} catch (Exception e) {
				// TODO: handle exception
			}
	    	return lst;
	    }

}
