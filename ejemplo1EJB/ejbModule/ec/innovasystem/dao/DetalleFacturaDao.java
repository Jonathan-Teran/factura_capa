package ec.innovasystem.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.com.innovasystem.entidades.DetalleFactura;




/**
 * Session Bean implementation class TipoProductoDao
 */
@Stateless
@LocalBean
public class DetalleFacturaDao extends DAO<DetalleFactura, Integer> {

    /**
     * Default constructor. 
     */
    public DetalleFacturaDao() {
        // TODO Auto-generated constructor stub
    }
    
    public void crearDetalleFactura (DetalleFactura detalleFactura){
    	try {
			crear(detalleFactura);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    public DetalleFactura consultarDetalleFactura(Integer id){
		try{
		return buscarPorPK(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
    @SuppressWarnings("unchecked")
	public List<DetalleFactura> consultarTodosDetalleFactura(){
		List<DetalleFactura> lst=null;
		try{
			Query q=getNamedQuery("DetalleFactura.findAll"); 
			lst=(List<DetalleFactura>) q.getResultList();
			}catch(Exception e){
				
			}
		return lst;
	}
    
	 @SuppressWarnings("unchecked")
		public List<DetalleFactura> buscarXNombre(int id){
	    	List<DetalleFactura> lst = null;
	    	try {
	    		Query q = getNamedQuery("DetalleFactura.buscarXNombre");
	    		q.setParameter("id", id);
				lst = (List<DetalleFactura>) q.getResultList();
			} catch (Exception e) {
				// TODO: handle exception
			}
	    	return lst;
	    }

}
