package ec.com.innovasystem.entidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_impuesto database table.
 * 
 */
@Entity
@Table(name="tipo_impuesto")
@NamedQuery(name="TipoImpuesto.findAll", query="SELECT t FROM TipoImpuesto t")
public class TipoImpuesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tipo_impuesto_id")
	private int tipoImpuestoId;

	private String descripcion;

	private String nombre;

	private int valor;

	//bi-directional many-to-one association to DetalleFactura
	@OneToMany(mappedBy="tipoImpuestoBean", fetch=FetchType.EAGER)
	private List<DetalleFactura> detalleFacturas;

	public TipoImpuesto() {
	}

	public int getTipoImpuestoId() {
		return this.tipoImpuestoId;
	}

	public void setTipoImpuestoId(int tipoImpuestoId) {
		this.tipoImpuestoId = tipoImpuestoId;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getValor() {
		return this.valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public List<DetalleFactura> getDetalleFacturas() {
		return this.detalleFacturas;
	}

	public void setDetalleFacturas(List<DetalleFactura> detalleFacturas) {
		this.detalleFacturas = detalleFacturas;
	}

	public DetalleFactura addDetalleFactura(DetalleFactura detalleFactura) {
		getDetalleFacturas().add(detalleFactura);
		detalleFactura.setTipoImpuestoBean(this);

		return detalleFactura;
	}

	public DetalleFactura removeDetalleFactura(DetalleFactura detalleFactura) {
		getDetalleFacturas().remove(detalleFactura);
		detalleFactura.setTipoImpuestoBean(null);

		return detalleFactura;
	}

}