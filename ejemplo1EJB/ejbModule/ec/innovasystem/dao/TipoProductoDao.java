package ec.innovasystem.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;


import ec.com.innovasystem.entidades.TipoProducto;


/**
 * Session Bean implementation class TipoProductoDao
 */
@Stateless
@LocalBean
public class TipoProductoDao extends DAO<TipoProducto, Integer> {

    /**
     * Default constructor. 
     */
    public TipoProductoDao() {
        // TODO Auto-generated constructor stub
    }
    
    public void crearTipoProducto (TipoProducto tipoProducto){
    	try {
			crear(tipoProducto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    public TipoProducto consultarTipoProducto(Integer id){
		try{
		return buscarPorPK(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoProducto> consultarTodosTipoProducto(){
		List<TipoProducto> lst=null;
		try{
			Query q=getNamedQuery("TipoProducto.findAll"); 
			lst=(List<TipoProducto>) q.getResultList();
			}catch(Exception e){
				
			}
		return lst;
	}
    

}
