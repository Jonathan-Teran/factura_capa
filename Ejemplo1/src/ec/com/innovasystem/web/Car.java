package ec.com.innovasystem.web;
import java.io.Serializable;



public class Car implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private int year;
	private String brand;
	private String color;
	private boolean sold;
	private int price;

	public Car() {
	}

	public Car(String id, int year, String brand, String color, boolean sold,
	int price) {
	this.id = id;
	this.year = year;
	this.brand = brand;
	this.color = color;
	this.sold = sold;
	this.price = price;
	}

	public String getId() {
	return id;
	}

	public void setId(String id) {
	this.id = id;
	}

	public int getYear() {
	return year;
	}

	public void setYear(int year) {
	this.year = year;
	}

	public String getBrand() {
	return brand;
	}

	public void setBrand(String brand) {
	this.brand = brand;
	}

	public String getColor() {
	return color;
	}

	public void setColor(String color) {
	this.color = color;
	}

	public boolean isSold() {
	return sold;
	}

	public void setSold(boolean sold) {
	this.sold = sold;
	}

	public int getPrice() {
	return price;
	}

	public void setPrice(int price) {
	this.price = price;
	}
	
	
}
