package ec.innovasystem.dao;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


public class DAO<E, PK> {
	@PersistenceContext(name="ejemplo1EJB")
	private EntityManager entityManager;
	
	
	/**
	 * Realiza la creacion de un nuevo registro por medio de persistencia
	 * @param entidad Representacion de la tabla para crear
	 * @throws Exception
	 */
	public void crear(E entidad) throws Exception{	
		try{
			getEntityManager().persist(entidad);
		}catch(Exception e){
			throw new Exception(e);
		}
	}

	/**
	 * Realiza la creacion la actualizacion de un registro por medio de persistencia
	 * @param entidad Representacion de la tabla para actualizar
	 * @return
	 * @throws Exception
	 */
	public E actualizar(E entidad) throws Exception{
		try{
			return getEntityManager().merge(entidad);
		}catch(Exception e){
			throw new Exception(e);
		}
	}

	/**
	 * Realiza la busqueda de un objeto por primary key
	 * @param pk Id a realizar la busqueda
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public E buscarPorPK(PK pk) throws Exception{
		try{
			ParameterizedType lTiposVariable= (ParameterizedType)this.getClass().getGenericSuperclass();
			Class lClase=(Class) lTiposVariable.getActualTypeArguments()[0];
			E entidad = (E) getEntityManager().find(lClase, pk);
			return entidad; 
		}catch(Exception e){
			throw new Exception(e);
		}
	}

	/**
	 * Realiza la eliminacion de un registro en la base de datos
	 * @param entidad Representacion de la tabla para eliminar
	 * @throws Exception
	 */
	public void eliminar(E entidad) throws Exception {
		getEntityManager().remove(actualizar(entidad));
	}
    
	/**
	 * Obtiene Obj de consulta de persistencia
	 * @param pNamedQuery Consulta a realizar
	 * @return
	 */
	protected Query getNamedQuery(String pNamedQuery) {
		return getEntityManager().createNamedQuery(pNamedQuery);
	}
	
	/**
	 * Obtiene Obj de consulta de Persistencia
	 * @param pQuery Consulta a realizar
	 * @return
	 */
	protected Query getQuery(String pQuery) {
		return getEntityManager().createQuery(pQuery);
	}
	
	/**
	 * Obtiene Obj de consulta nativa
	 * @param pNamedQuery Consulta a realizar
	 * @return
	 */
	protected Query getNativeQuery(String pNamedQuery) {
		return getEntityManager().createNativeQuery(pNamedQuery);
	}	
	
	/**
	 * Obtiene Obj de consulta de persistencia y obteniendo el numero de registro
	 * @param pNamedQuery Consulta a realizar
	 * @param pFirstResult Numero de registro
	 * @return
	 */
	protected Query getNamedQuery(String pNamedQuery,int pFirstResult) {
		return getNamedQuery(pNamedQuery).
			   setFirstResult(pFirstResult);
	}
	/**
	 * Obtiene Obj de consulta de persistencia y obteniendo el numero de registro y la cantidad maxima
	 * @param pNamedQuery Consulta a realizar
	 * @param pFirstResult Numero de registro
	 * @param pMaxResults Cantidad maxima de registros a obtener
	 * @return
	 */
	protected Query getNamedQuery(String pNamedQuery,int pFirstResult,int pMaxResults) {
		return getNamedQuery(pNamedQuery,pFirstResult).
			   setMaxResults(pMaxResults);
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
