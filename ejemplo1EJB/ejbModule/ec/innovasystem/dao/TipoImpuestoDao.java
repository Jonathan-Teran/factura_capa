package ec.innovasystem.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.com.innovasystem.entidades.TipoImpuesto;



@Stateless
@LocalBean
public class TipoImpuestoDao extends DAO<TipoImpuesto, Integer> {

    /**
     * Default constructor. 
     */
    public TipoImpuestoDao() {
        // TODO Auto-generated constructor stub
    }
    
    public void crearTipoImpuesto (TipoImpuesto tipoImpuesto){
    	try {
			crear(tipoImpuesto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    public TipoImpuesto consultarTipoImpuesto(Integer id){
		try{
		return buscarPorPK(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<TipoImpuesto> consultarTodosTipoImpuesto(){
		List<TipoImpuesto> lst=null;
		try{
			Query q=getNamedQuery("TipoImpuesto.findAll"); 
			lst=(List<TipoImpuesto>) q.getResultList();
			}catch(Exception e){
				
			}
		return lst;
	}
    

}
