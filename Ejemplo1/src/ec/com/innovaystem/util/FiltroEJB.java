package ec.com.innovaystem.util;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class FiltroEJB {
	
	private static ThreadLocal<Boolean> marcaRollback=new ThreadLocal<Boolean>();
	
	@AroundInvoke
	public Object interceptarErrores(InvocationContext pInvocationContext)
	throws Exception
	{
		try
		{
			return pInvocationContext.proceed();
		}
		catch(Exception e)
		{
			marcaRollback.set(true);
			throw e;
		}
	}
	
	public static boolean isMarcaRollback()
	{
		Boolean lMarca=marcaRollback.get();
		return lMarca!=null && lMarca;
	}
	public static void limpiarMarcaRollback()
	{
		marcaRollback.set(null);
	}
}