package ec.innovasystem.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.com.innovasystem.entidades.Marca;



/**
 * Session Bean implementation class TipoProductoDao
 */
@Stateless
@LocalBean
public class MarcaDao extends DAO<Marca, Integer> {

    /**
     * Default constructor. 
     */
    public MarcaDao() {
        // TODO Auto-generated constructor stub
    }
    
    public void crearMarca (Marca marca){
    	try {
			crear(marca);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    public Marca consultarMarca(Integer id){
		try{
		return buscarPorPK(id);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Marca> consultarTodosMarca(){
		List<Marca> lst=null;
		try{
			Query q=getNamedQuery("Marca.findAll"); 
			lst=(List<Marca>) q.getResultList();
			}catch(Exception e){
				
			}
		return lst;
	}
    

}
